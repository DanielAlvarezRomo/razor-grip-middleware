import { Router } from 'express';
import ctrl from './messages.ctrl';

export const messagesRouter = Router();

messagesRouter
  .get('/:room_id', ctrl.getMessages)
  ; 