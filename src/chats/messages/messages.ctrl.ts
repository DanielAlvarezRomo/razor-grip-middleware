import { Request, Response } from 'express'; 
import { MEDIUMINT } from 'sequelize';
import {Message} from './messages.model';

class MessagesCtrl {
  public async getMessages(req: Request, res: Response): Promise<any> {
    try {
      const uid = req.headers.uid;
      const { room_id } = req.params;
      const model = new Message({room_id: String(room_id), to: String(uid) });
      const get = await model.getMessages();
      res.status(get.code).json({ data: get.data});
    } catch (error) { 
      res.status(500).json(error);
    }
  }
  
  
}

export default new MessagesCtrl();
