import { Router } from 'express';
import { roomsRouter } from './rooms/rooms.router';
import { messagesRouter } from './messages/messages.router';

export const chatsRouter = Router();

chatsRouter
  .use('/rooms', roomsRouter)
  .use('/messages', messagesRouter)
  ;