import { Router } from 'express';
import ctrl from './rooms.ctrl';

export const roomsRouter = Router();

roomsRouter
  .post('/:to', ctrl.newRoom)
  ;