import cors from 'cors';
import express, { Application } from 'express';
import { createServer } from 'http';
const morgan = require('morgan');
import { Server } from 'socket.io';
import socket from './socket';
import { mainRouter } from './main/main.router';

export const app: Application = express();
export const http = createServer(app);
export const io = new Server(http, { cors: { origin: '*' } });

// webpack
/*
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackConfig = require('./webpack.config')
*/

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan('short'));
app.use(cors());
/*
app.use(express.static(__dirname + '/public'))
app.use(webpackDevMiddleware(webpack(webpackConfig)));
*/
io.on('connection', socket);


app.use('/api', mainRouter);
