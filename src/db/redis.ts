import Redis from 'ioredis';
import { REDIS_HOST } from '../config/env';

const client = new Redis(6379, REDIS_HOST);
export default client;