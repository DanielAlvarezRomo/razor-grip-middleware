import { Router } from 'express';
import ctrl from './users.ctrl';

export const usersRouter = Router();

usersRouter
  .post('/add', ctrl.addUser)
  .patch('/block', ctrl.blockUser)
  .patch('/unblock', ctrl.unblockUser)
  .get('/', ctrl.getUsers)
  ;